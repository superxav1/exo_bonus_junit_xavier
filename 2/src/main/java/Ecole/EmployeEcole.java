package Ecole;

public interface EmployeEcole {
    double getSalaire();
    void setSalaire(double salaireAugment);
    int getNbAbsMoisEnCours();
    void setNbAbsMoisEnCours(int nbAbsMoisEnCours);
    String sePresenter();
//    public void demanderAugmentation(Ecole ecole, EmployeEcole e, int SalaireAugment);
}

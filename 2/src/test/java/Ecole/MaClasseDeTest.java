package Ecole;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class MaClasseDeTest {
	
	@Test
	public void testEleve() {
		
		Eleve eleve = new Eleve("Housseyn", "BOUDJENANE", 36,NiveauClasse.CE2);
		
		//V�rifie si la valeur d'absent est correcte
		eleve.setAbsent(true);
		assertEquals(true, eleve.getAbsent());
		//System.out.println("Valeur attendue : "+eleve.getAbsent()+" Valeur obtenue : "+eleve.getAbsent());
		
		
		//V�rifie si la nouvelle valeur d'absent est correcte
		eleve.setAbsent(false);
		assertEquals(false, eleve.getAbsent());
		//System.out.println("Valeur attendue : "+eleve.getAbsent()+" Valeur obtenue : "+eleve.getAbsent());		
	
		
	}
	
	
	
	

}

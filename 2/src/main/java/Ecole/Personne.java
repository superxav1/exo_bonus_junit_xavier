package Ecole;

public class Personne {
    protected String nom, prenom;
    protected int salaire;
    protected int nbAbsMoisEnCours;

    public Personne(String nom, String prenom) {
        this.nom = nom;
        this.prenom = prenom;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public double getSalaire() {
        return salaire;
    }

    public void setSalaire(int salaire) {
        this.salaire = salaire;
    }

    public int getNbAbsMoisEnCours() {
        return nbAbsMoisEnCours;
    }

    public void setNbAbsMoisEnCours(int nbAbsMoisEnCours) {
        this.nbAbsMoisEnCours = nbAbsMoisEnCours;
    }

    public String sePresenter() {
        String tmpString = "Je m'appelle " + nom + " " + prenom + ".";
        System.out.println(tmpString);
        return tmpString;
    }
}


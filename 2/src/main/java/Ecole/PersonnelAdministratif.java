package Ecole;

public class PersonnelAdministratif extends Personne{
	private String nom, prenom;
    private int salaire;
    private int nbAbsMoisEnCours;
    
    //Constructeur du personnel administratif
    public PersonnelAdministratif(String prenom, String nom) {
        super(prenom, nom);
    }

	
	//Setters et Getters du personnel administratif
	public int getSalaire() {
		return salaire;
	}

	public void setSalaire(int salaire) {
		this.salaire = salaire;
	}

	public int getNb_abs_mois_en_cours() {
		return nbAbsMoisEnCours;
	}

	public void setNb_abs_mois_en_cours(int nb_abs_mois_en_cours) {
		this.nbAbsMoisEnCours = nb_abs_mois_en_cours;
	}
	
	
	public String sePresenter() {
		return super.sePresenter() + " je suis personnel administratif et je gagne "+salaire;
	}
	
//	public void demanderAugmentation(Ecole ecole, EmployeEcole e, double SalaireAugment) {
//		ecole.augmenterGens(e, SalaireAugment);
//		
//	}

}

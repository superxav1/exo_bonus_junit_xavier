package Ecole;

public class Enseignant extends Personne implements EmployeEcole {
//	  private String nom, prenom;
    private int salaire;
    private int nbAbsMoisEnCours;
    
    //Constructeur d'enseignant
	public Enseignant(String prenom, String nom) {
        super(prenom, nom);
    }

	
	//Setters et Getters d'enseignant
	public double getSalaire() {
		return salaire;
	}

	public void setSalaire(int salaire) {
		this.salaire = salaire;
	}

	public int getNb_abs_mois_en_cours() {
		return nbAbsMoisEnCours;
	}

	public void setNb_abs_mois_en_cours(int nb_abs_mois_en_cours) {
		this.nbAbsMoisEnCours = nb_abs_mois_en_cours;
	}
	
	
	public String sePresenter() {
		return super.sePresenter() + " je suis enseignant et je gagne "+salaire;
	}


//	public void demanderAugmentation(Ecole ecole, EmployeEcole e, double SalaireAugment) {
//		ecole.augmenterGens(e, SalaireAugment);
//		
//	}


	public void setSalaire(double salaireAugment) {
		// TODO Auto-generated method stub
		
	}



		

    
 

}

package Ecole;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.Set;

import org.junit.Test;

import org.junit.Test;

public class ClasseTestEnseignantAdministratif {
	


	@Test
	public void testEnseignant() {
		
		
		Enseignant enseignant = new Enseignant("TIOUALE", "Ayoub");
		enseignant.setSalaire(50000);
		//v�rifie que le salaire de l'enseignant est �gal � 5000
		assertEquals(50000, enseignant.getSalaire());
		
		enseignant.setNb_abs_mois_en_cours(40);
		//v�rifie que le nombre de jours d'absence correspond bien � 40
		assertEquals(40, enseignant.getNb_abs_mois_en_cours());
		
		
		PersonnelAdministratif pa = new PersonnelAdministratif("BOUDJENANE", "Housseyn");
		pa.setSalaire(1000);
		//v�rifie que le salaire du personnel administratif est �gal � 1000
		assertEquals(1000, pa.getSalaire());
		
		pa.setNb_abs_mois_en_cours(1);
		//v�rifie que le nombre de jours d'absence correspond bien � 1
		assertEquals(1, pa.getNb_abs_mois_en_cours());
		
		//v�rifie que la m�thode se pr�senter retourne le bon message
		enseignant.sePresenter();
		pa.sePresenter();
		assertEquals("Je m'appelle TIOUALE Ayoub. je suis enseignant et je gagne 50000",enseignant.sePresenter());
		
		//v�rifie la m�thode augmenter salaire
		Ecole ecole1 = new Ecole();
		
		ecole1.augmenterEns(enseignant, 8000);
		ecole1.augmenterAdmin(pa, 2000);
		assertEquals(8000, enseignant.getSalaire());
		assertEquals(2000, pa.getSalaire());
		
		
		
	}
	
	@Test
	public void Polymorphe() {
	    Personne enseignant = new Enseignant("Monsieur", "Ayoub");
	    enseignant.setSalaire(1500);
	    Personne pa2 = new PersonnelAdministratif("Monsieur", "Housseyn");
	    pa2.setSalaire(1000); 
	    Personne eleve = new Eleve("Housseyn", "BOUDJENANE", 16,NiveauClasse.CP);

	    // Appeler la m�thode sePresenter de mani�re polymorphique
	    enseignant.sePresenter();
	    pa2.sePresenter();
	    eleve.sePresenter();
	    
	    assertEquals("Je m'appelle Monsieur Ayoub. je suis enseignant et je gagne 1500", enseignant.sePresenter());
	    assertEquals("Je m'appelle Monsieur Housseyn. je suis personnel administratif et je gagne 1000", pa2.sePresenter());
	}
	
	@Test
	public void TestAge() {
	    Eleve eleve2 = new Eleve("Housseyn", "BOUDJENANE", 7,NiveauClasse.CP);
	    
	    Eleve eleve3 = new Eleve("Ayoub", "Hussin", 19, NiveauClasse.CE2);
	    
	    //V�rifie que l'�ge est bien compris entre 6 et 10
	    int ageOK = eleve2.getAge();
        assertTrue(ageOK >= 6 && ageOK <= 10);
        
        int agePasOK = eleve3.getAge();
        assertFalse(agePasOK >= 6 && agePasOK <= 10);
	} 
}
	
//	@Test
//	public void testAugmentationSalaire() {
//		Enseignant enseignant = new Enseignant("Hussin","Ayoub");
//		PersonnelAdministratif pa1 = new PersonnelAdministratif("Ayoubbe", "Housseine");
//		Ecole ecole = new Ecole();
//		
//		enseignant.setNb_abs_mois_en_cours(10);
//		pa1.setNb_abs_mois_en_cours(4);
//		
//		enseignant.demanderAugmentation(ecole, enseignant, 20000);
//		pa1.demanderAugmentationPA(ecole, pa1, 1000);
//		
//		assertTrue(enseignant.getSalaire() == 0);
//		assertTrue(pa1.getSalaire() == 2500);
//	}
//}

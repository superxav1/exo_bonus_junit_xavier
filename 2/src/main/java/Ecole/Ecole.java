	package Ecole;

//La classe est "final" ici car il est demand� dans l'�nonc� qu'elle soit immuable

public final class Ecole {
	
	public void augmenterEns(Enseignant prof, int nvsalaire){
		prof.setSalaire(nvsalaire);
	}
	
	public void augmenterAdmin(PersonnelAdministratif pa, int nvsalaire){
		pa.setSalaire(nvsalaire);
	}
	
	public void augmenterGens(PersonnelAdministratif pa1, int salaireAugment) {
		if(pa1.getNbAbsMoisEnCours()<=4) {
		pa1.setSalaire(salaireAugment);
		}
		else {
		}
	}
	
	public void classeEleve(Eleve e) {
		switch(e.getAge()) {
		case 6 : e.setNiveau_Classe(NiveauClasse.CP); break;
		case 7 : e.setNiveau_Classe(NiveauClasse.CE1); break;
		case 8 : e.setNiveau_Classe(NiveauClasse.CE2); break;
		case 9 : e.setNiveau_Classe(NiveauClasse.CM1); break;
		case 10 : e.setNiveau_Classe(NiveauClasse.CM2); break;
		}
	}
}
